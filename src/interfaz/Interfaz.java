/*Copyright [2020] [Alberto Daguerre Torres, Jesús Joana Azuara, Daniel Fernández López, Martin Berlanas Molera copyright]
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
*/

/*
 * En esta clase se crea la base de nuestro programa, donde se desarrollan  "procesar petición", "inicializar fichero" e "inicializar catálogo".
 * @author Alberto Daguerre
 * @author Jesús Joana
 * @author Daniel Fernández
 * @author Martin Berlanas
 * @version final 16/12/2020
 */

package interfaz;
import dominio.*;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;


/**
 * Se definen los atributos de la clase Interfaz.
 */
public class Interfaz{
    private static String HELP_TEXT = "Si quiere añadir un móvil: java -cp bin aplicacion.Principal add marca modelo precio" + " \nSi quiere ver la lista de los móviles añadidos al catálogo: java -cp bin aplicacion.Principal list"
            + "\nSi quiere borrar un móvil del catálogo: java -cp bin aplicación.Principal borrar marca modelo precio" + "\nSi quiere modificar un móvil del catálogo: java -cp bin aplicacion.Principal modificar marca1 modelo1 precio1 marca2 modelo2 precio2"
            + "\n " + "\nPuede acceder al menu Help cuando lo desee";
    private static String NOMBRE_FICHERO = "catalogoDeMoviles.txt";
    private static String NOMBRE_CSV = "catalogoDeMoviles.csv";


    /**
     *
     * @param sentencia
     */
    public static void procesarPeticion(String sentencia) {
        String[] args = sentencia.split(" ");
        Catalogo catalogo = inicializarCatalogo(NOMBRE_FICHERO);
        if (args[0].equals("help")) {
            System.out.println(HELP_TEXT);
        } else if (args[0].equals("list")) {
            if (catalogo.toString().equals("")) System.out.println("No hay ningún modelo en el catálogo");
            else {
                System.out.println(catalogo);
            }
        } else if (args[0].equals("add")) {
            Marca marca = new Marca(args[1], args[2], Integer.parseInt(args[3]));
            catalogo.annadirMarca(marca);
            inicializarFichero(catalogo);
	    if(args.length >= 5){
		    System.out.println("Hay demasiados parámetros. Solo puede haber  marca, modelo y precio del móvil");
	    } else if (args.length < 4){
		    System.out.println("No hay parámetros suficientes. Debe haber marca, modelo y precio del móvil");
	    }
        } else if (args[0].equals("csv")) {
            iniciarCsv(catalogo);
            if (args.length >= 5) {
                System.out.println("Hay demasiados parámetros. Solo puede haber  marca, modelo y precio del móvil");
            } else if (args.length < 4) {
                System.out.println("No hay parámetros suficientes. Debe haber marca, modelo y precio del móvil");
            }
        } else if (args[0].equals("borrar")) {
            Marca borrarMarca = new Marca(args[1], args[2], Integer.parseInt(args[3]));
            catalogo.borrarProducto(borrarMarca);
            inicializarFichero(catalogo);
            iniciarCsv(catalogo);
            if (args.length >= 5) {
                System.out.println("Hay demasiados parámetros. Solo puede haber marca, modelo y precio del móvil");
            } else if (args.length < 4) {
                System.out.println("No hay parámetros suficientes. Debe haber marca, modelo y precio del móvil");
            }
        } else if (args[0].equals("modificar")) {
            Marca marcaAntigua = new Marca(args[1], args[2], Integer.parseInt(args[3]));
            Marca marcaNueva = new Marca(args[4], args[5], Integer.parseInt(args[6]));
            catalogo.cambiarMovil(marcaAntigua, marcaNueva);
            inicializarFichero(catalogo);
            iniciarCsv(catalogo);
            if (args.length >= 8) {
                System.out.println("Hay demasiados parámetros. Solo puede haber: marca1 modelo1 precio1 marca2 modelo2 precio2 (móvil1=móvil existente/móvil2=móvil nuevo)");
            } else if(args.length < 7) {
                System.out.println("No hay parámetros suficientes. Debe haber: marca1 modelo1 precio1 marca2 modelo2 precio2 (móvil1=móvil existente/móvil2=móvil nuevo)");
            }
        } else if (args[0].equals("csv")) {
            iniciarCsv(catalogo);
        }
    }

    private static void inicializarFichero(Catalogo catalogo){
        try{
            FileWriter fw = new FileWriter(NOMBRE_FICHERO);
            fw.write(catalogo.toString());
            fw.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    private static void iniciarCsv(Catalogo catalogo){
        try{
            FileWriter fw = new FileWriter(NOMBRE_CSV);
            fw.write(catalogo.toCsv());
            fw.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    private static Catalogo inicializarCatalogo(String nombreFichero){
        Catalogo catalogo = new Catalogo();
        try{
            File file = new File(nombreFichero);
            Scanner sc = new Scanner(file);
            while(sc.hasNext()){
                String modelo = sc.next();
                String caracteristicas = sc.next();
                int precio = Integer.parseInt(sc.next());
                Marca marca = new Marca(modelo, caracteristicas, precio);
                catalogo.annadirMarca(marca);
            }
            sc.close();
        } catch (FileNotFoundException e){
            inicializarFichero(catalogo);
        } catch (Exception e){
            System.out.println(e);
        }
        return catalogo;
    }
}
