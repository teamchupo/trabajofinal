/*
Copyright [2020] [Alberto Daguerre Torres, Jesús Joana Azuara, Daniel Fernández López, Martin Berlanas Molera copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * Clase principal del programa el cual procesa los datos.
 * @user Alberto Daguerre
 * @user Jesús Joana
 * @user Daniel Fernández
 * @user Martín Berlanas
 * @versión final 16/12/2020
 */

package aplicacion;
import interfaz.Interfaz;

public class Principal{
	
	/**
	 * Se trata de una función para ejecutar una clase desde el terminal sin necesidad de crear una petición.
	 */
	public static void main(String[] args){
		String sentencia = "";
		for(int i = 0; i< args.length; i++){
			sentencia += args[i] + " ";
		}
		Interfaz.procesarPeticion(sentencia);
	}
}
