/*
Copyright [2020] [Alberto Daguerre Torres, Jesús Joana Azuara, Daniel Fernández López, Martin Berlanas Molera copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
	http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Esta clase contiene los objetos que definen la marca, las características y el precio de los moviles del catálogo.
 * @author Alberto Daguerre
 * @author Daniel Fernández
 * @author Martin Berlanas
 * @author Jesús Joana
 * @version final 16/12/2020
 */

package dominio;


/**
 * Define la clase y sus atributos.
 */
public class Marca extends Catalogo{
        private String modelo;
        private String caracteristicas;
	private int precio;

	/**
	 * Constructor de la clase
	 */
	public Marca(){
		modelo = "";
		caracteristicas = "";
		precio = Integer.parseInt("");	
        }

	/**
	 *
	 * @param modelo
	 * @param caracteristicas
	 * @param precio
	 */
        public Marca(String modelo, String caracteristicas, int precio){
                this.modelo = modelo;
                this.caracteristicas = caracteristicas;
		this.precio = precio;
	}


 	/**
	 * Lo que hace este método es devolver un modelo de movil.
	 * @return devuelve el nombre del modelo.
	 */
 	public String getModelo(){
 		return modelo;
 	}

	/**
	 * Método interno que genera un modelo.
	 * @param this sirve para definir e inicializar un nuevo modelo.
	 */
	public void setModelo(String modelo){
                this.modelo = modelo;
        }

        /** Lo que hace este método es devolver una característica del móvil.
		 * @param return devuelve las características
	    */

        public String getCaracteristicas(){
                return caracteristicas;
        }

        /** Método interno que genera unas características.
         * @param this sirve para definir e inicializar unas nuevas características.
         */

        public void setCaracteristicas(String caracteristicas){
                this.caracteristicas = caracteristicas;
    	} 
	
    	/**
		 * Lo que hace este método es devolver el precio del móvil.
         * @param return devuelve el precio del móvil
         */

		public int getPrecio(){
		return precio;
	}

		/**
		 * Método interno que genera un precio.
         * @param precio sirve para definir e inicializar un nuevo precio.
         */

	public void setPrecio(int precio){
		this.precio = precio;
	}


	/**
	 * Sirve para devolver la información creada por el constructor.
	 * @return
	 */
	public String toString(){
		return modelo + " " + caracteristicas + " " + precio + "\n";
	}

	/**
	 *
	 * @return
	 */
	public String toCsv(){
        return modelo + "," + caracteristicas + "," + precio + "\n";
    }
}

