/*
Copyright [2020] [Alberto Daguerre Torres, Jesús Joana Azuara, Daniel Fernández López, Martin Berlanas Molera copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Esta clase contiene el método toString y el método "annadirMarca" y la creación de un ArrayList para que se añadan las marcas a medida que el usuario vaya añadiendo más.
 * @author Alberto Daguerre
 * @author Jesús Joana
 * @author Daniel Fernández
 * @author Martín Berlanas
 * @version final 16/12/2020
 */

package dominio;
import java.util.ArrayList;
public class Catalogo {

        private ArrayList<Marca> coleccionMarcas = new ArrayList<>();

        /**
         * @param marca Método para añadir marcas al catálogo
         */

        public void annadirMarca(Marca marca) {
                coleccionMarcas.add(marca);
        }

        /**
         * @param nuevaMarca método para borrar productos del catálogo
         */

        public void borrarProducto(Marca nuevaMarca) {
                boolean borrado = false;
                for (int i = coleccionMarcas.size() - 1; i >= 0; i--) {
                        if(coleccionMarcas.get(i).toString().equals(nuevaMarca.toString())){
                        coleccionMarcas.remove(i);
                        borrado = true;
                }
        }
         if(borrado =false){
                System.out.println(coleccionMarcas.toString() + "No existe el movil en el catálogo");
        }
}
        public void cambiarMovil(Marca marcaAntigua, Marca marcaNueva){
                boolean modificar = false;
                for (int i = coleccionMarcas.size()-1; i >= 0; i--){
                        if (coleccionMarcas.get(i).toString().equals(marcaAntigua.toString())){
                                coleccionMarcas.set(i, marcaNueva);
                                modificar = true;
                        }
                }if(modificar = false){
                        System.out.println(marcaAntigua.toString() + "No existe el móvil en el catálogo");
                }
        }

        /**
         * Método toString que devuelve los datos
         * @return datos Datos de la marca
         */
	public String toString(){
                StringBuilder datos = new StringBuilder();
                for (Marca marca : coleccionMarcas){
                        datos.append(marca);
                }
                return datos.toString();
        }

        /**
         * Método toString que devuelve los datos
         * @return datos Datos de la marca
         */
	public String toCsv(){
        StringBuilder datos = new StringBuilder();
        for (Marca marca : coleccionMarcas){
            datos.append(marca);
        }
        return datos.toString();
    }
}
