![copyright1.jpg](copyright1.jpg)

/*Copyright [2020] [Alberto Daguerre Torres, Jesús Joana Azuara, Daniel Fernández López, Martin Berlanas Molera copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.*/
    
    

# Participantes:

* Martín Berlanas
* Alberto Daguerre
* Daniel Fernández
* Jesús Joana

# Objetivo:

Este programa se ha creado para poder almacenar y visualizar los datos de los móviles dentro de un catálogo. Va dirigido especialmente a una empresa de telefonía móvil. En este se podrá apreciar la marca, el modelo, y el precio de los dispositivos. 

# Funcionalidad:

* "java -cp bin aplicacion.Principal" para poder inicializar el programa.

* "java -cp bin aplicacion.Principal add marca modelo precio" para añadir un móvil al catálogo.

* "java -cp bin aplicacion.Principal borrar marca modelo precio" para eliminar un móvil del catálogo.

* "java -cp bin aplicacion.Principal modificar marca1 modelo1 precio1 marca2 modelo2 precio2" para modificar diferentes valores de un móvil del catálogo.

* "java -cp bin aplicacion.Principal list" para ver los móviles añadidos al catálogo.

* "java -cp bin aplicacion.Principal help" para acceder al menú de ayuda en cualquier momento.
